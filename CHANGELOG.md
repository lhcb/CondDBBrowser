# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased][]

## [0.1.3][] - 2023-02-24

### Fixed
- Fix version of shiboken2 to match that of PySide2 (!6)

## **YANKED** [0.1.2][] - 2023-02-24

### Fixed
- Fix build of RPM in gitlab-ci (!5)
- Support for latest git behaviour wrt ownership of repository files (!4)
- Propagate command line arguments through contrib shell wrapper (!3)

## [0.1.1][] - 2019-01-02
Patch release only to introduce automatic deployment of RPM for LHCb.

### Added
- Add build and deployment of RPM via Gitlab-CI (!2)

## 0.1.0 - 2019-01-02
Initial version

### Added
- Import CondDBBrowser code from LHCb (!1)


[Unreleased]: https://gitlab.cern.ch/lhcb/CondDBBrowser/compare/0.1.3...master
[0.1.3]: https://gitlab.cern.ch/lhcb/CondDBBrowser/compare/0.1.2...0.1.3
[0.1.2]: https://gitlab.cern.ch/lhcb/CondDBBrowser/compare/0.1.1...0.1.2
[0.1.1]: https://gitlab.cern.ch/lhcb/CondDBBrowser/compare/0.1.0...0.1.1
